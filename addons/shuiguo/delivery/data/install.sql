--
-- 表的结构 `addon_delivery_config`
--

CREATE TABLE `addon_delivery_config` (
  `id` int(11) unsigned NOT NULL,
  `delivery_time` text NOT NULL,
  `status` int(11) NOT NULL COMMENT '1:开启0:关闭',
  `remark` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `addon_delivery_config`
--

INSERT INTO `addon_delivery_config` (`id`, `delivery_time`, `status`, `remark`, `created_at`, `updated_at`) VALUES
(1, '10:30-11:30,14:30-15:30,16:00-17:00,15:30-17:30', 0, '12', '2017-03-01 09:15:15', '2017-03-02 02:26:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_delivery_config`
--
ALTER TABLE `addon_delivery_config`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_delivery_config`
--
ALTER TABLE `addon_delivery_config`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;